package mobileAlertsToMqtt

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"strings"
)

func main() {
	//~~~~~~~~~~~~~~~~~~~~~~//
	// Make an HTTP request //
	//~~~~~~~~~~~~~~~~~~~~~~//
	data := url.Values{}
	data.Set("phoneid", os.Getenv("MOBILE_ALERTS_PHONE_ID"))

	resp, _ := http.Post("http://measurements.mobile-alerts.eu/", "application/x-www-form-urlencoded", strings.NewReader(data.Encode()))
	bytes, _ := ioutil.ReadAll(resp.Body)

	fmt.Println("HTML:\n\n", string(bytes))

	resp.Body.Close()
}
